(function ($) {
    $(document).ready(function () {
        $('.navigator__drop').on('click', function () {
            $('.navigator__link').toggleClass('test').slideDown(1000);
            if (!$('.navigator__link').hasClass('test')) {
                $('.navigator__link').slideUp(1000);
            }
        });

        $(window).on('resize', function () {
           setTimeout(function () {
               if($(window).width() >= 992) {
                   $('.navigator__link').css({
                       display: 'flex'
                   });
               }
               else {
                   $('.navigator__link').css({
                       display: 'none'
                   });
               }
           }, 100)
        });
    });
})(jQuery)
